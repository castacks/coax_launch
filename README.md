# README #

### What is this repository for? ###

This package aims to run ```lidar_ekf``` on ODROID_XU4 (Ubuntu 14.04, ROS Indigo) with g2 and microstrain IMU.

### How do I get set up? ###

Install dependencies: 
If you installed ROS-base, then following packages need to be installed separately. 
```
sudo apt-get install ros-indigo-octomap ros-indigo-octomap-mapping ros-indigo-mavlink libgdal1-dev libproj-dev ros-indigo-cv-bridge ros-indigo-diagnostic-updater ros-indigo-angles ros-indigo-eigen-conversions ros-indigo-tf-conversions ros-indigo-image-transport ros-indigo-urdf ros-indigo-laser-geometry ros-indigo-control-toolbox ros-indigo-realtime-tools ros-indigo-urg-node ros-indigo-robot-state-publisher
```

Create workspace ```test_ws```, fetch repositories and build: 

```bash 
sudo apt-get install python-wstool # get wstool
source /opt/ros/indigo/setup.bash # init environment
mkdir -p test_ws/src
cd test_ws/src
catkin_init_workspace
git clone git@bitbucket.org:castacks/metarepository.git
ln -s metarepository/rosinstall/coax_localization.rosinstall .rosinstall
wstool info # see status
wstool up # get stuff
cd ..
catkin_make -DCMAKE_BUILD_TYPE=Release --make-args -j1 # -j1 argument is to avoid building errors
```

### How do I run? ###
Each time to generate a new map, localize in the map and logging data, a new ```testName``` should be given such that old files won't be replaced. For example, in a new test case with name ```nsh_test```:

To mapping:

```bash
roslaunch coax_mapping.launch testName:=nsh_test
```

Make sure the g2 is static while mapping. Please be patient while the node is processing map after scanning. The launch process should end itself when mapping completed.

To localize:
```bash
roslaunch coax_localization.launch testName:=nsh_test
```

Map files are stored as ```$(find lidar_ekf)/map/nsh_test.bt```, and logging foler is ```$(find coax_launch)/dat/nsh_test/```. And bagfiles are named according to timestamp.

For interface with a pixhawk, also launch ```$(find mavros)/launch/px4.launch```. Then the pixhawk send back local position to topic ```/mavros/local_position/pose```.


### Who do I talk to? ###
Weikun Zhen (zhenwk@gmail.com)

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2015, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.